#!/usr/bin/env python
import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello')

message = 'Hello wabbit!'


channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=message)
print("[x] Sent %r" % (message,))

connection.close()



